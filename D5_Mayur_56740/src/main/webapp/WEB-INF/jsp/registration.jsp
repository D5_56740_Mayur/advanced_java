<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Edit Tutorial</h2>
	<sf:form modelAttribute="vendor" method="post" action="/registration">
		<table>
			<tr>
				<td>Id</td>
				<td><sf:input path="id"/>
				<td></td>
			</tr>
			<tr>
				<td>Name</td>
				<td><sf:input path="name" />
				<td></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><sf:input path="email"/>
				<td></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><sf:input path="password"/>
				<td></td>
			</tr>
			<tr>
				<td>City</td>
				<td><sf:input path="city" />
				<td></td>
			</tr>
			<tr>
				<td>Contact no</td>
				<td><sf:input path="cellNo"/>
				<td></td>
			</tr>
			<tr>
				<td>Registration Amount</td>
				<td><sf:input path="regAmount"/>
				<td></td>
			</tr>
			<tr>
				<td>Registration Date</td>
				<td><sf:input path="regDate"/>
				<td></td>
			</tr>
			<tr>
				<td>Role</td>
				<td><sf:input path="role"/>
				<td></td>
			</tr>
			
			<tr>
				<td></td>
				<td><input type="submit" value="Register"/></td>
				<td></td>
			</tr>
		</table>
	</sf:form>	
	<hr/>
		<a href="/manage">Return</a>
</body>
</html>