<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Vendor Details</h2>
<table border="1"colour="red">
		<thead>
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Email</td>
				<td>City</td>
				<td>Role</td>
				<td>Contact No</td>
				<td>Action</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="vendor" items="${vendorList}">
				<tr>
					<td>${vendor.id}</td>
					<td><a href="/details?id=${vendor.id}">${vendor.name}</a></td>
					<td>${vendor.email}</td>
					<td>${vendor.city}</td>
					<td>${vendor.role}</td>
					<td>${vendor.cellNo}</td>
					<td>
						<a href="/update?id=${vendor.id}">Update</a>				
					</td>
					<td>
						<a href="/delete?id=${vendor.id}">Delete</a>				
					</td>
				</tr>				
			</c:forEach>
		</tbody>
		</table>
		<a href="/registration">Add New Vendor</a>
</body>
</html>