<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Details</title>
</head>
<body>
	Hello, ${vendor.name} <hr/>
	
	<h2>Your Details</h2>
	<h3>ID: ${vendor.id}</h3>
	<h3>NAME: ${vendor.name}</h3>
	<h3>EMAIL: ${vendor.email}</h3>
	<h3>CITY ${vendor.city}</h3>
	<h3>CONTACT NO:${vendor.cellNo}</h3>
	<h3>YOUR ROLE: ${vendor.role}</h3>
	

	<a href="/logout">Sign Out</a>
</body>
</html>