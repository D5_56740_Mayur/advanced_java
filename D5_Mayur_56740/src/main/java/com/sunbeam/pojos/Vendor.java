package com.sunbeam.pojos;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Date;
@Entity
@Table(name = "vendors")
public class Vendor {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String name;
	@Column
	private String email;
	@Column
	private String password;
	@Column
	private String city;
	@Column(name="cell_no")
	private String cellNo;
	@Column(name="reg_amount")
	private double regAmount;
	@Column(name="reg_date")
	private Date regDate;
	@Column
	private String role;
	
	
	public Vendor() {
	}


	public Vendor(int id, String name, String email, String password, String city, String cellNo, double regAmount,
			Date regDate, String role) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.city = city;
		this.cellNo = cellNo;
		this.regAmount = regAmount;
		this.regDate = regDate;
		this.role = role;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCellNo() {
		return cellNo;
	}


	public void setCellNo(String cellNo) {
		this.cellNo = cellNo;
	}


	public double getRegAmount() {
		return regAmount;
	}


	public void setRegAmount(double regAmount) {
		this.regAmount = regAmount;
	}


	public Date getRegDate() {
		return regDate;
	}


	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	@Override
	public String toString() {
		return "Vendor [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", city=" + city
				+ ", cellNo=" + cellNo + ", regAmount=" + regAmount + ", regDate=" + regDate + ", role=" + role + "]";
	}


	
	
}
