package com.sunbeam.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sunbeam.pojos.Credentials;
import com.sunbeam.pojos.Vendor;
import com.sunbeam.services.VendorServiceImpl;

@Controller
public class VendorControllerImpl1 {
	private VendorServiceImpl vendorService;

	public VendorControllerImpl1(VendorServiceImpl vendorService) {
		super();
		this.vendorService = vendorService;
	}

	@RequestMapping("/index") 
	public String index() {
		
		return "index";}
	
	@RequestMapping("/login") 
	public String login(Model model) {
		Credentials cred=new Credentials("abc@gmail.com","");
		model.addAttribute("command", cred);
		return "login";
	}
	@RequestMapping("/validate")
	public String validate(Credentials cred,HttpSession session) {
		Vendor vendor = vendorService.authenticateUser(cred.getEmail(), cred.getPassword());
		if(vendor == null)
			return "failed";
		session.setAttribute("vendor", vendor);
		if(vendor.getRole().equals("admin"))
			return "redirect:manage";
		return "redirect:details";
	}
	@RequestMapping("/details")
	public String details(String email,Model model,HttpSession session) {
		Vendor vendor = vendorService.findByEmail(email);
		model.addAttribute("vendor",vendor);
		return "details"; 
	}
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
	
		session.invalidate();
		return "logout";
	}
	@RequestMapping("/manage")
	public String manage(Model model) {
		List<Vendor> list = vendorService.findAllVendors();
		model.addAttribute("vendorList", list);
		return "manage"; // --> manage.jsp
	}
	@RequestMapping("/registration")
	public String registration(Vendor u,Model model) {
		 vendorService.saveVendor(u);
		model.addAttribute("vendor",u );
		return "registration"; // --> manage.jsp
	}
	@RequestMapping("/delete")
	public String delete(Vendor u,Model model) {
		vendorService.deleteUser(u);
		model.addAttribute("vendor" );
		return "redirect:manage"; // --> manage.jsp
	
	
	}

	@GetMapping("/update")
	public String edit(@RequestParam("id") int vendorId,Model model) {
		Vendor vendor=vendorService.findById(vendorId);
		model.addAttribute("vendor",vendor);
		return "update";
	}
	
    @PostMapping("/update")
    public String update(Vendor vendor) {
     Vendor u=	vendorService.saveVendor(vendor);
    	return"update";
    	
    	
    }
}