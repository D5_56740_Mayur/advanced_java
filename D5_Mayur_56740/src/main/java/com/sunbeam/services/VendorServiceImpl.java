package com.sunbeam.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.sunbeam.daos.VendorDao;
import com.sunbeam.pojos.Vendor;

@Transactional
@Service
public class VendorServiceImpl {
	
private VendorDao vendorDao;

public VendorServiceImpl(VendorDao vendorDao) {
	super();
	this.vendorDao = vendorDao;
}
//public Optional<Vendor> findById(int id) {//change
//	return vendorDao.findById(id);
//}
public Vendor findByEmail(String email) {
	return vendorDao.findByEmail(email);
}
public Vendor authenticateUser(String email, String password) {
	Vendor user = vendorDao.findByEmail(email);
	if(user != null && user.getPassword().equals(password))
		return user;
	return null;
}
public List<Vendor> findAllVendors() {
	return vendorDao.findAll();
}
public Vendor saveVendor(Vendor u) {
	return vendorDao.save(u);
}

public void deleteUser(Vendor vendor) {
vendorDao.delete(vendor);
}
public Vendor updateVendor(Vendor u) {
	return vendorDao.save(u);
}
public Vendor findById(int id) {
	Optional<Vendor>b=vendorDao.findById(id);
	return b.orElse(null);
}
//public Vendor findUserById(int id) {
	
//	return vendorDao.findById(null);
//}

}
